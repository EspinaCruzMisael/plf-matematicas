# Conjuntos Aplicaciones y Funciones (2002)
```plantuml
@startmindmap
+[#Gold] Conjuntos Aplicaciones y Funciones
++[#lime] conjuntos 
+++_ son 
++++[#Orange] un conjunto no se puede definir ya que solo se da por intituiba y por explicada 
+++++_ tienen
++++++[#lightGray] caracteristicas 
+++++++_ como
++++++++[#Azure] el conjunto esta ligado a los elementos 
++++++++[#Azure] no se puede definir la relacion de conjunto y elemento 
+++++++++[#Azure] ya que dado la relacion se sabe si el elemento esta en el conjunto
++++++++[#Azure]  entre ellos tienen una relacion de pertenecia 
++++++++[#Azure] esto significa que dado la relacion siempre se sabe si el elemento esta o no en el conjunto  
++++++[#lightGray] inclusion de conjuntos 
+++++++_ es
++++++++[#Red]  la relacion de orden que hay entre numeros  
++++++[#lightGray] operaciones basicas
+++++++_ como
++++++++[#Pink] intercepcion 
+++++++++[#violet] elemetos que pertenecen simultaneamente a ambos 
++++++++[#Pink] union 
+++++++++[#violet] elemetos que pertenecen a algunos de ellos 
++++++++[#pink] complementacion 
+++++++++[#violet] los que no pertenecen complementario a un conjunto dado 
++++++++[#pink] diferencia de conjuntos 
+++++++++[#violet] es el resultado de otro conjunto con los elementos del primer conjunto sin los elementos del segundo conjunto
++++++[#lightGray] tipos de conjuntos
+++++++_ son 
++++++++[#lime] universal
+++++++++[#white] es un conjunto de referencia en el que ocurren toda la teoria  
++++++++[#lime] vacio
+++++++++[#white] es una necesidad logica para cerrar el conjunto que no tiene elementos 
++++++[#lightGray] recursos
+++++++_ como 
++++++++[#Plum] diagrama de venn 
+++++++++[#tan] son los elementos que forman parten o que tienen un caracteristica en comun 
+++++++++[#tan] ayudan a lo que es la intercepcion, union y el complementario 
++++++[#lightGray] cardinalidad de un conjunto 
+++++++[#red] es el numero de elementos que foman el conjunto 
++++++[#lightGray] formulas para resolverlo
+++++++_ son
++++++++[#red] el cardinal de la union de conjuntos  
++++++++[#red] acotacion e decardinales de conjuntos 
++[#lime] aplicaciones y funciones  
+++_ conlleva a
++++[#orange] tranformacion o cambio 
+++++[#lime] ejemplos
++++++_ como 
+++++++[#lightgreen] decisiones del gobierno 
+++++++[#lightgreen] biologia 
++++++++[#lightgreen] al enseñarnos como evolucionan los seres 
++++[#yellow] aplicaciones
+++++_ se definen por 
++++++[#tan] una tranformacion o una regla que convierte a cada uno de los \nelementos determinados de un conjunto a en un unico elemento de un conjunto final b
++++++[#tan] todos los elementos del primer conjunto tienen un tranformado en el segundo conjunto y este tranformado debe ser unico
++++++[#tan] la aplicacion conlleva a una clase de transformacion
++++++[#tan] imagen 
+++++++[#lime] terreno de los valores
++++++[#tan] imagen inversa
+++++++[#lime] como el conjunto toca a otro conjunto 
++++++[#tan] tipos de aplicaciones 
+++++++_ como
++++++++[#lime] inyectiva 
++++++++[#lime] subyectiva
++++++++[#lime] viyectiva 
++++[#yellow] funciones 
+++++[#tan] conjuntos de numeros que se transforman  
++++++_ se define por
+++++++[#Silver] cuando se tiene una transformacion o una aplicaion entre conjuntos de numeros 
+++++++[#Silver] son aplicaciones que son mas faciles de ver 
+++++++[#Silver] grafica de la funcion 
++++++++[#lightgray] puede ser una linea recta 
++++++++[#lightgray] pueden ser numeros reales 
++++++++[#lightgray] figuras complejas
+++++++[#Silver] es una representacion gráfica que permite conocer\n intuitivamente el comportamiento de la función
@endmindmap
```

# Funciones (2010)
```plantuml
@startmindmap
+[#lime] Funciones 
++_ es
+++[#white] una aplicacion especial de una tranformaciones de un conjunto con otro conjunto
++++_ contiene
+++++[#red] relacion 
++++++_ con
+++++++[#pink] es el corazon de las matematicas 
+++++++[#pink] son las matematicas del cambio 
+++++++[#pink] aplicaciones 
++++++++_ ya que 
+++++++++[#turquoise] son tranformaciones de un conjunto a otro conjunto 
+++++++++[#turquoise] de una situacion a otra situacion  
+++++++[#pink] es un tema para intentar comprender el entorno\n y para intentar con ayuda de las matematicas resolver los problemas cotidianos
+++++++[#pink] las funciones se enfrenta a la situacion del cambio con respecto al tiempo 
++++++++_ como
+++++++++[#turquoise] temperatura en un lugar 
+++++++++[#turquoise] cambio entre el euro y el dolar 
+++++++++[#turquoise]  el afan del hombre es ver que matematicas hay debajo del cambio
+++++++[#pink] una funcion que a cada numero se le asigna su cuadrado 
+++++[#red] representaciones 
++++++_ como
+++++++[#pink] plano de descartes 
++++++++[#lightgreen] se representa el eje de los numeros realeas en una recta
++++++++[#lightgreen] se obtine una curva 
+++++++++[#turquoise] con ella nos da una idea de como es la funcion 
+++++++++[#turquoise] como cambia el conjunto primero en el conjunto segundo 
+++++[#red] caracteristicas 
++++++_ como
+++++++[#yellow] crecientes 
++++++++[#lightgreen] cuando aumenta las variables independientes
+++++++[#yellow] decrecientes 
++++++++[#lightgreen] cuando a medida que el valor de la variable independiente aumenta el valor de la función disminuye
+++++++[#yellow] intuitiva
++++++++[#lightgreen] maximos 
+++++++++[#silver] va creciendo y pasa por el maximo
++++++++[#lightgreen] minimos 
+++++++++[#silver] va decreciendo y pasa de maximo a ser minimo
+++++[#red] limites 
++++++_ tienen
+++++++[#violet] es que cuando un valor de una varia esta cerca del punto 
+++++++[#violet] como se aproxima los valores de una funcion al punto de interes
+++++++[#violet] continuidad 
++++++++_ es
+++++++++[#violet] una funcion que no produce la discontinuidad 
+++++++++[#violet] son funciones mas manejables 
+++++[#red] derivada
++++++_ cualidades
+++++++[#tan] sus calculos son 
++++++++[#tan] rutinarios
++++++++[#tan] secillos 
++++++++[#tan] son iguales  
+++++++[#tan] es como aproximar una funcion relativamente complicada con una funcion mas simple
+++++++[#tan] es la pendiente de la recta tangente 
+++++++[#tan] son atajos a las funciones 
+++++++[#tan] aproximacion al cambio 
+++++++[#tan] se resuelve mediante una funcion lineal 
+++++++[#tan] es eso que mide el cambio 
++++++++[#tan] ya que sustituye una idea compleja por una mas simple 
+++++++[#tan] la aproximacion local de una funcion 
+++++++[#tan] es un inveto que sale 
++++++++_ en
+++++++++[#tan] la economia 
+++++++++[#tan] las ciencias sociales 
+++++++++[#tan] en otras muchas aplicaciones  
@endmindmap
```
# La matemática del computador (2002)
```plantuml
@startmindmap
+[#lightGReen] La matemática del computador
++_ contienen 
+++[#azure] cualidades
++++_ como   
+++++[#tan] las matematicas estan en los cimientos de los computadores 
+++++[#tan] son una base para el desarrollo de los computadores 
+++++[#tan] sistema que utiliza el computador
+++++[#tan] representacion practica 
+++[#azure] aritmetica del computador 
++++_ atributos  
+++++[#silver] las matematicas ayudan a ser una representacion habil de los numeros con infinitas cifras decimales 
+++++[#silver] numero aproximado 
++++++[#lightgreen] son los valores que no son exactamente lo que corresponde a la cifra dada
+++++[#silver] error 
+++++[#silver] digitos significativos 
++++++[#lightgreen] son los digitos que proporcionan informacion sobre la magnitud del signo 
+++++[#silver] truncar 
++++++_ realiza
+++++++[#lime] es cortar un numero que tiene una exprecion muy largar
+++++++[#lime] un ejemplo es cuando se corta pi que se deja en 3.14159
+++++[#silver] redondeo 
++++++_ realiza
+++++++[#pink] es un truncamiento mas elegante 
+++++++[#pink] evita que los errores cometidos sean lo menor posible 
+++[#azure] conforma 
++++_ por
+++++[#red] sistema binario 
++++++[#orange]  facil de representar en el mundo fisico 
++++++[#orange] se enlaza con la logica booleana 
++++++[#orange] es un sistema muy simple 
++++++[#orange] y es dificil que tenga una dificultad 
++++++[#orange] se enlaza con la pertenecia y no pertenecia de conjuntos
++++++[#orange] se identifica como 
+++++++[#lime] el 0 como el no paso de corriente 
+++++++[#lime] el 1 como el paso de corriente
+++++++[#lime] como un interruptor  
+++++++[#lime] con el 1 y 0 se contruye todo un sistema de numeracion
+++++[#red] se construye la digitalizacion 
++++++[#orange] donde es la representacion de 
+++++++[#lime] imagenes 
+++++++[#lime] numeros 
+++++++[#lime] etc
+++++[#red] se utilza por el paso de corriente como un interruptor 
+++++[#red] se arman estructuras complejas 
+++++[#red] sistema octal y hexagesimal 
++++++[#orange] son representaciones compatas de numeros muy empleadas 
++++++[#orange] se usan menos simbolos que con el sistema binario 
+++[#azure] hace
++++_ es
+++++[#lime] como se manejan los numeros muy grandes o muy pequeños del ordenador 
+++++[#lime] se respresetan mediante el producto de un numero por la potencia de 10
+++++[#lime] representacion 
++++++[#gold] se hace mediante la codificacion 
+++++++_ donde se ilustra 
++++++++[#pink] las letras 
++++++++[#pink] los signos de puntuacion 
++++++++[#pink] los numeros numeros 
+++++[#lime] aritmetica en punto flotante 
++++++[#gold] como se pueden sumar, multiplicar los numeros que se representan en una notacion cientifica

@endmindmap
```
